Simple Checkout System
======================

![Codeship Status for joh_schmidt/checkout](https://codeship.com/projects/146635d0-2512-0134-4072-3a1ce86cc27d/status?branch=master)

Simple Checkout System ist eine sehr einfache Implementierung eines Checkout für den Warenkorb eines Händlers mit verschiedenen Preisschemata.

# Ausführen der Tests

Das Projekt basiert auf Maven. Die Tests werden daher wie folgt gestartet:

```
git clone https://bitbucket.org/joh_schmidt/checkout.git
cd checkout
mvn test
```

# Anmerkungen

* Die Tests in ```TestPrice``` decken die Funktionalitäten aller Klassen ab; auf weitere Tests wurde daher verzichtet.
* Es finden keine Validierungen oder Plausibilitätsprüfungen statt (Ausnahme null-Check in ``` CheckOut.calculatePrice()```). Z.B. wird am Ende einer Preisberechnung nicht überprüft, ob evtl. noch nicht in Rechnung gestellte Produkte vorhanden sind (``` Balance.unbilledUnits > 0```).