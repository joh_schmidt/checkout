package checkout;

/**
 * A {@code PricingStrategy} for multipriced items.
 */
public class SpecialPrice implements PricingStrategy {
    private final int quantity;
    private final int price;

    /**
     * @param quantity number of units needed to get the {@code SpecialPrice}
     * @param price    price per {@code number} of units
     */
    public SpecialPrice(final int quantity, final int price) {
        this.price = price;
        this.quantity = quantity;
    }

    public void apply(final Balance currentBalance) {
        if (currentBalance.getUnbilledUnits() >= quantity) {
            final int discountableUnits = currentBalance.getUnbilledUnits() - (currentBalance.getUnbilledUnits() % quantity);
            currentBalance.clearing(discountableUnits, discountableUnits / quantity * price);
        }
    }
}
