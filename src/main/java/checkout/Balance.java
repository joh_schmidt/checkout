package checkout;

/**
 * Shows the current state of transforming unbilled units in to the bill.
 * Similar to the transformation of current assets to accounts receivable in a balance sheet (accounting).
 */
public class Balance {
    private int unbilledUnits;
    private int currentBill;

    public Balance(final int unbilledUnits) {
        this.unbilledUnits = unbilledUnits;
        this.currentBill = 0;
    }

    public int getUnbilledUnits() {
        return unbilledUnits;
    }

    public int getCurrentBill() {
        return currentBill;
    }

    /**
     * Clearing of parts of units.
     *
     * @param billedUnits  number of now billed units to subtract from unbilledUnits
     * @param billedAmount new billed amount to add to the current bill
     */
    public void clearing(final int billedUnits, final int billedAmount) {
        unbilledUnits -= billedUnits;
        currentBill += billedAmount;
    }
}
