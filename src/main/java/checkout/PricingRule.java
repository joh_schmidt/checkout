package checkout;

import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * Uses different pricing strategies to calculate the total price of all units.
 * The different strategies will be applied in the predefined order.
 */
public class PricingRule {
    private final List<PricingStrategy> pricingStrategies;

    public PricingRule(final PricingStrategy... strategies) {
        pricingStrategies = ImmutableList.copyOf(strategies);
    }

    public int calculatePrice(final int quantity) {
        final Balance currentBalance = new Balance(quantity);

        pricingStrategies.forEach(pricingStrategy -> pricingStrategy.apply(currentBalance));

        return currentBalance.getCurrentBill();
    }
}
