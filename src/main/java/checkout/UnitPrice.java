package checkout;

/**
 * A {@code PricingStrategy} for single units.
 */
public class UnitPrice implements PricingStrategy {
    private final int price;

    public UnitPrice(final int price) {
        this.price = price;
    }

    public void apply(Balance currentBalance) {
        if (currentBalance.getUnbilledUnits() > 0) {
            currentBalance.clearing(currentBalance.getUnbilledUnits(), currentBalance.getUnbilledUnits() * price);
        }
    }
}