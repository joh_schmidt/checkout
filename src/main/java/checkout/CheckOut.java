package checkout;

import com.google.common.util.concurrent.AtomicLongMap;

import java.util.Map;

/**
 * Simple checkout system in a supermarket for a pricing schema.
 * Each item (identified by Stock Keeping Units or SKU) has its own {@code PricingRule}.
 */
public class CheckOut {
    private final AtomicLongMap<String> items = AtomicLongMap.create();
    private final Map<String, PricingRule> pricingRules;

    public CheckOut(final Map<String, PricingRule> rule) {
        this.pricingRules = rule;
    }

    public void scan(final String stockKeepingUnit) {
        items.getAndIncrement(stockKeepingUnit);
    }

    public int total() {
        int total = 0;
        for (final Map.Entry<String, Long> item : items.asMap().entrySet()) {
            total += calculatePrice(item.getKey(), Math.toIntExact(item.getValue()));
        }

        return total;
    }

    private int calculatePrice(final String stockKeepingUnit, final int count) {
        final PricingRule pricingRule = pricingRules.get(stockKeepingUnit);
        if (pricingRule == null) {
            throw new IllegalArgumentException(String.format("SKU %s not found", stockKeepingUnit));
        }

        return pricingRule.calculatePrice(count);
    }
}
