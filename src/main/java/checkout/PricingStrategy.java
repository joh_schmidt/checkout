package checkout;

public interface PricingStrategy {
    /**
     * Apply the {@code PricingStrategy} to as many unbilled units as possible.
     *
     * @param currentBalance The current balance of unbilledUnits and the bill (total price of already billed units).
     */
    void apply(Balance currentBalance);
}
