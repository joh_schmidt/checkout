package checkout;

import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertEquals;

public class TestPrice {
    private static final Map<String, PricingRule> rule = ImmutableMap.of(
            "A", new PricingRule(new SpecialPrice(3, 100), new UnitPrice(40)),
            "B", new PricingRule(new SpecialPrice(2, 80), new UnitPrice(50)),
            "C", new PricingRule(new UnitPrice(25)),
            "D", new PricingRule(new UnitPrice(20))
    );

    public int calculatePrice(String goods) {
        CheckOut co = new CheckOut(rule);
        for (int i = 0; i < goods.length(); i++) {
            co.scan(String.valueOf(goods.charAt(i)));
        }
        return co.total();
    }

    @Test
    public void totals() {
        assertEquals(0, calculatePrice(""));
        assertEquals(40, calculatePrice("A"));
        assertEquals(90, calculatePrice("AB"));
        assertEquals(135, calculatePrice("CDBA"));
        assertEquals(80, calculatePrice("AA"));
        assertEquals(100, calculatePrice("AAA"));
        assertEquals(140, calculatePrice("AAAA"));
        assertEquals(180, calculatePrice("AAAAA"));
        assertEquals(200, calculatePrice("AAAAAA"));
        assertEquals(150, calculatePrice("AAAB"));
        assertEquals(180, calculatePrice("AAABB"));
        assertEquals(200, calculatePrice("AAABBD"));
        assertEquals(200, calculatePrice("DABABA"));
    }

    @Test
    public void incremental() {
        CheckOut co = new CheckOut(rule);
        assertEquals(0, co.total());
        co.scan("A"); assertEquals(40, co.total());
        co.scan("B"); assertEquals(90, co.total());
        co.scan("A"); assertEquals(130, co.total());
        co.scan("A"); assertEquals(150, co.total());
        co.scan("B"); assertEquals(180, co.total());
    }
}
